﻿using UnityEngine;
using System.Collections;

public class EnemyBase : MonoBehaviour {

	EnemySpawner spawnScript;

	float health = 5;
	float maxHealth = 5;
	bool damaged = false;
	// Use this for initialization
	void Start () {
		spawnScript = GetComponent<EnemySpawner>();
	}

	IEnumerator Destruction(){
		yield return null;
	}
	
	public void Killed(){
		StartCoroutine("Destruction");
	}

	IEnumerator Retaliate(){
		while(GameControl.isPlaying && true){
			GameObject enemy = spawnScript.GetRandomEnemy();
			if(enemy!=null){
				enemy.SendMessage("Fire");
			}
			yield return new WaitForSeconds(0.5f);
		}
		yield return null;
	}

	public void TakeDamage(){
		if(!damaged){
			StartCoroutine("Retaliate");
		}
		health -= 1;
		if(health <= 0){
			BroadcastMessage("Killed");
			//Destroy(gameObject);
		}
		//StartCoroutine("Destruction");
		
	}
}
