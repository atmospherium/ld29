﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraScript : MonoBehaviour {

	GameObject player;

	Dictionary<string, Vector2> cameraSettings = new Dictionary<string, Vector2>(){
		{"Dead",new Vector2(-8.7f,169.5f)},
		{"Offset",new Vector2(-8.82f,138.3f)},
		{"Offset2",new Vector2(-9.36f,138.3f)},
		{"Closeup",new Vector2(-9.81f,87.8f)},
		{"Standard",new Vector2(-15.43f,132.7f)},
		{"Far",new Vector2(-293.1f,31.5f)},
		{"Teleport",new Vector2(-23.93f,1)}


		/*
		{"Dead",new Vector2(-7.61f,125.5f)},
		{"Dead2",new Vector2(-7.77f,131.9f)},
		{"Rectangular",new Vector2(-7.95f,132.5f)},
		{"Offset",new Vector2(-8.88f,134.4f)},
		{"Offset2",new Vector2(-9.00f,140.1f)},
		{"Closeup",new Vector2(-9.81f,87.8f)},
		{"Standard",new Vector2(-11.43f,133.7f)},
		{"Far",new Vector2(-293.1f,12.5f)},
		{"Teleport",new Vector2(-23.93f,1)}
		*/
	};

	public string initPos = "Teleport";

	// Use this for initialization
	void Awake () {
		player = GameObject.Find("Player");
		transform.position = new Vector3(transform.position.x, transform.position.y, cameraSettings[initPos].x);
		Camera.main.fieldOfView = cameraSettings[initPos].y;
	}

	int transitionLength = 2;
	IEnumerator TransitionCamera(string cameraSetting){
		if(cameraSetting == "Teleport"){
			transform.position = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
		}
		float currentZ = transform.position.z;
		float currentFOV = Camera.main.fieldOfView;
		int pos = 0;
		while(pos<=transitionLength){
			Vector3 currentPos = new Vector3(transform.position.x,transform.position.y, currentZ);
			Vector3 targetPos = new Vector3(currentPos.x, currentPos.y, cameraSettings[cameraSetting].x);
			transform.position = Vector3.Lerp(currentPos, targetPos, (float)pos/transitionLength);

			Camera.main.fieldOfView = Mathf.Lerp(currentFOV, cameraSettings[cameraSetting].y, (float)pos/transitionLength);
			pos++;
			yield return null;
		}
		yield return null;
	}

	public void Transition(string cameraSetting, int length){
		transitionLength = length;
		StopCoroutine("TransitionCamera");
		StartCoroutine("TransitionCamera",cameraSetting);
	}
		              
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 currentPos = transform.position;
		Vector3 targetPos = new Vector3(player.transform.position.x, player.transform.position.y, currentPos.z);

		transform.position = Vector3.Lerp(currentPos, targetPos, 4*Time.deltaTime);

	}
}
