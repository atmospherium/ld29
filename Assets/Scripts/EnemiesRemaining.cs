﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class EnemiesRemaining : MonoBehaviour {

	TextMesh textMesh;
	void Start(){
		textMesh = GetComponent<TextMesh>();
	}

	// Update is called once per frame
	void Update () {
		textMesh.text = "Enemies Remaining: "+GameControl.enemiesRemaining.ToString();
	}
}
