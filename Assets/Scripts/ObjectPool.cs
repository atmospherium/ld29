﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

	public GameObject bullet;
	public GameObject enemy;

	List<GameObject> projectiles = null;
	private int numberOfProjectiles = 3;

	List<GameObject> enemies = null;
	private int numberOfEnemies = 3;

	void Awake(){
		projectiles = new List<GameObject>();
		enemies = new List<GameObject>();
		InstantiateProjectiles();
		InstantiateEnemies();
	}

	void Update(){
		if(Input.GetMouseButtonDown(1)){
			ActivateProjectile();
		}
	}

	void InstantiateProjectiles(){
		for(int i = 0; i < numberOfProjectiles; i++){
			projectiles.Add(Instantiate(bullet) as GameObject);
			projectiles[i].transform.parent = transform;
			projectiles[i].SetActiveRecursively(false);
		}
	}

	void InstantiateEnemies(){
		for(int i = 0; i < numberOfProjectiles; i++){
			enemies.Add(Instantiate(enemy) as GameObject);
			enemies[i].gameObject.name = "Enemy"+i.ToString();
			enemies[i].transform.parent = transform;
			enemies[i].SetActiveRecursively(false);
		}
	}

	public GameObject ActivateProjectile(){
		int i = 0;
		while(i < projectiles.Count){
			if(projectiles[i].active == false){
				projectiles[i].SetActiveRecursively(true);
				return projectiles[i];

			}
			i++;
		}
		projectiles.Add(Instantiate(bullet) as GameObject);
		projectiles[i].transform.parent = transform;
		projectiles[i].SetActiveRecursively(true);
		return projectiles[i];
	}

	public GameObject ActivateEnemy(){
		int i = 0;
		while(i < enemies.Count){
			if(enemies[i].active == false){
				enemies[i].SetActiveRecursively(true);
				return enemies[i];
				
			}
			i++;
		}
		enemies.Add(Instantiate(enemy) as GameObject);
		enemies[i].gameObject.name = "Enemy"+i.ToString();
		enemies[i].transform.parent = transform;
		enemies[i].SetActiveRecursively(true);
		return enemies[i];
	}
}