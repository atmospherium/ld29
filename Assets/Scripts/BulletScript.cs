﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {

	public void SetRotation(Quaternion rotation){
		transform.rotation = rotation;
	}

	// Update is called once per frame
	void Update () {
		if(Mathf.Abs(transform.position.x)>150||Mathf.Abs(transform.position.y)>80){
			gameObject.SetActiveRecursively(false);
		}
		rigidbody2D.velocity = transform.right * 50;

	}

	void OnCollisionEnter2D(Collision2D c){
		int cLayer = c.gameObject.layer;
		int bLayer = gameObject.layer;

		if(GameControl.isPlaying && cLayer!=bLayer && c.gameObject.name != gameObject.name){
			c.gameObject.SendMessage("TakeDamage");
			if(gameObject.activeSelf){
				gameObject.SetActiveRecursively(false);
			}
		}
	}

	public void Activate(Quaternion rotation){
		transform.rotation = rotation;
	}
}
