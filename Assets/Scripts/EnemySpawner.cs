﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {
	ObjectPool objectPool;

	int enemyCount = 20;
	public int enemiesRemaining;

	float gridRange = 12;
	float zRange = -3;

	System.Random random;

	public List<GameObject> enemies = new List<GameObject>();

	public AudioClip[] hitSounds;

	GameObject player;

	void Start(){

		player = GameObject.Find("Player");

		random = new System.Random();

		objectPool = GameObject.Find("ObjectPool").GetComponent<ObjectPool>();
		for(var i = 0; i < enemyCount; i++){

			float randX = (float)(random.NextDouble()*2)-1;
			float randY = (float)(random.NextDouble()*2)-1;
			float randZ = (float)random.NextDouble();

			GameObject enemy = objectPool.ActivateEnemy();
			enemy.transform.parent = transform;
			enemy.transform.localPosition = new Vector3(randX*gridRange,randY*gridRange,randZ*zRange);

			enemies.Add(enemy);

			EnemyScript enemyScript = enemy.GetComponent<EnemyScript>();
			enemyScript.player = player;
			enemyScript.parentScript = this;

		}
	
	}

	public GameObject GetRandomEnemy(){
		if(enemies.Count>0){
			return enemies[random.Next (enemies.Count)];
		}else{
			return null;
		}
	}

	public void EnemyKilled(GameObject enemy){
		enemy.SetActiveRecursively(false);
		enemy.transform.parent = objectPool.transform;
		enemies.Remove(enemy);
		GameControl.enemiesKilled++;
		if(enemies.Count<1){
			GameControl.enemiesKilled++;
			Destroy(gameObject);
		}

		audio.PlayOneShot(hitSounds[random.Next(hitSounds.Length)]);

	}

	// Update is called once per frame
	void Update () {
		enemiesRemaining = enemies.Count + 1;
		transform.Rotate(Vector3.forward * 15 * Time.deltaTime);
	}
}
