﻿using UnityEngine;
using System.Collections;

public class LayerControl : MonoBehaviour {

	public float speed = 20;
	public Vector3[] rotDir = new Vector3[3]{Vector3.right,Vector3.up,Vector3.forward};

	public bool rotation = true;
	public bool jitter = false;
	public bool spread = false;

	public bool hasAudioSource;
	public AudioClip[] glitchSounds;

	GameObject redObj;
	GameObject greenObj;
	GameObject blueObj;

	System.Random random;

	public float offsetAmount = 0.2f;

	bool spreading = false;

	Vector3 redObjOriginal;
	Vector3 redObjTarget;
	
	Vector3 greenObjOriginal;
	Vector3 greenObjTarget;

	// Use this for initialization
	void Start () {
		redObj = (GameObject)transform.FindChild("Red").gameObject;
		greenObj = (GameObject)transform.FindChild("Green").gameObject;
		blueObj = (GameObject)transform.FindChild("Blue").gameObject;

		random = new System.Random();

		hasAudioSource = GetComponent<AudioSource>() != null;

		redObjOriginal = redObj.transform.localPosition;
		redObjTarget = new Vector3(redObjOriginal.x, redObjOriginal.y, 0.02f);
		
		greenObjOriginal = greenObj.transform.localPosition;
		greenObjTarget = new Vector3(greenObjOriginal.x, greenObjOriginal.y, -0.02f);
	}

	public void SetRotation(bool isTrue){
		rotation = isTrue;
	}

	public void SetJitter(bool isTrue){
		jitter = isTrue;
	}



	void Spread(){
		float sqrVol = Mathf.Clamp(Mathf.Pow(AudioInfo.vol,2)-0.125f,0,1);
		redObj.transform.localPosition = Vector3.Lerp(redObjOriginal,redObjTarget,sqrVol);
		greenObj.transform.localPosition = Vector3.Lerp(greenObjOriginal,greenObjTarget,sqrVol);
	}


	void Jitter(){
		bool play = false;
		float threshold = 0.98f;

		float randX = (float)(random.NextDouble()*2)-1;
		float randY = (float)(random.NextDouble()*2)-1;

		if(Mathf.Abs(randX)>threshold){

			redObj.transform.localPosition = new Vector3(randX*offsetAmount,randY*offsetAmount,0);
			blueObj.transform.localPosition = new Vector3(-randX*offsetAmount,-randY*offsetAmount,0);

			play = true;
		}
		if(glitchSounds.Length > 0 && hasAudioSource && play){
			audio.PlayOneShot(glitchSounds[(int)(random.NextDouble()*glitchSounds.Length)]);
		}
	}

	// Update is called once per frame
	void Update () {
		if(spread){
			Spread();
		}
		if(rotation){
			redObj.transform.Rotate(rotDir[0]*speed*Time.deltaTime);
			greenObj.transform.Rotate(rotDir[1]*speed*Time.deltaTime);
			blueObj.transform.Rotate(rotDir[2]*speed*Time.deltaTime);
		}
		if(jitter){
			Jitter();
		}
	}
}
