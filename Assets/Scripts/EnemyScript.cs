﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EnemyScript : MonoBehaviour {

	GameObject redObj;
	GameObject blueObj;
	GameObject greenObj;

	PolygonCollider2D collider;

	public EnemySpawner parentScript;

	List<GameObject> order = new List<GameObject>();
	System.Random random = new System.Random();

	public GameObject player;

	ObjectPool objectPool;
	GameObject bulletSpawn;
		

	// Use this for initialization
	void Start () {

		collider = GetComponent<PolygonCollider2D>();

		redObj = (GameObject)transform.FindChild("Red").gameObject;
		greenObj = (GameObject)transform.FindChild("Green").gameObject;
		blueObj = (GameObject)transform.FindChild("Blue").gameObject;

		order.Add(redObj);
		order.Add(greenObj);
		order.Add(blueObj);

		objectPool = GameObject.Find("ObjectPool").GetComponent<ObjectPool>();

		bulletSpawn = GameObject.Find(gameObject.name+"/BulletSpawn");


	}
	
	// Update is called once per frame
	void Update () {
		if(!GameControl.isPlaying ) return;
		transform.LookAt(player.transform.position);

	}

	void Fire(){
		if(!GameControl.isPlaying) return;
		GameObject bullet = objectPool.ActivateProjectile();
		bullet.transform.position = bulletSpawn.transform.position;
		bullet.layer = gameObject.layer;
		BulletScript bulletScript = bullet.GetComponent<BulletScript>();
		bulletScript.SetRotation(transform.rotation);
	}
	
	IEnumerator ExtremeJitter(){
		for (int i = 0; i < order.Count; i++) {
			GameObject temp = order[i];
			int randomIndex = Random.Range(i, order.Count);
			order[i] = order[randomIndex];
			order[randomIndex] = temp;
		}
		while(GameControl.isPlaying && true){
			float randX = (float)random.NextDouble();
			float randY = (float)random.NextDouble();
			redObj.transform.localPosition = new Vector3(randX*0.5f,randY*0.5f,0);
			
			randX = (float)random.NextDouble();
			randY = (float)random.NextDouble();
			greenObj.transform.localPosition = new Vector3(randX*0.5f,randY*0.5f,0);
			
			randX = (float)random.NextDouble();
			randY = (float)random.NextDouble();
			blueObj.transform.localPosition = new Vector3(randX*0.5f,randY*0.5f,0);
			yield return new WaitForSeconds(0.01f);
		}
		yield return null;
	}

	IEnumerator Destruction(){
		collider.enabled = false;
		StartCoroutine("ExtremeJitter");
		audio.Play();
		foreach(GameObject orderObject in order.ToList()){
			orderObject.SetActive(false);
			order.Remove(orderObject);
			yield return new WaitForSeconds(0.5f);
		}
		parentScript.EnemyKilled(gameObject);
		yield return null;

	}

	public void Killed(){
		StartCoroutine("Destruction");
	}

	public void TakeDamage(){
		StartCoroutine("Destruction");

	}
}
