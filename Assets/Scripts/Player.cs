﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	GameObject targeter;
	GameObject bulletSpawn;

	CameraScript camScript;
	
	CircleCollider2D collider;

	bool boosting = false;

	bool isDead = false;

	float _speed = 20;
	float _speedMod = 4f;
	float Speed {
		get{
			if(boosting){
				return _speed*_speedMod;
			}else{
				return _speed;
			}
		}
	}

	System.Random random = new System.Random();

	public AudioClip[] fireSounds;
	public AudioClip deathClip;
	
	float vel = 0;
	float prevVel = 0;

	bool canFire = true;
	bool _fireBullets = false;
	bool fireBullets {
		get{
			return canFire && _fireBullets;
		}
		set{
			_fireBullets = value;
		}
	}
	
	ObjectPool objectPool;
	GameObject bullets;

	GameObject background;
	
	// Use this for initialization
	void Awake () {
		Application.runInBackground = false;
		Physics.IgnoreLayerCollision(8,9);
		
		collider = gameObject.GetComponent<CircleCollider2D>();
		
		objectPool = GameObject.Find("ObjectPool").GetComponent<ObjectPool>();
		
		targeter = GameObject.Find("Targeter");
		bulletSpawn = GameObject.Find(gameObject.name+"/Targeter/BulletSpawn");
		
		camScript= GameObject.Find ("Main Camera").GetComponent<CameraScript>();

		background = GameObject.Find ("Background");
	}

	void Start(){
		camScript.Transition(cameraArray[currentCamSetting],80);
	}

	public IEnumerator Fire(){

		while(fireBullets){
			GameObject bullet = objectPool.ActivateProjectile();
			bullet.transform.position = bulletSpawn.transform.position;
			bullet.layer = gameObject.layer;
			BulletScript bulletScript = bullet.GetComponent<BulletScript>();
			bulletScript.SetRotation(targeter.transform.rotation);

			audio.PlayOneShot(fireSounds[random.Next(fireSounds.Length)]);

			yield return new WaitForSeconds(0.1f);
		}
		yield return null;
	}

	public string[] cameraArray = new string[2]{"Teleport","Rectangular"};

	IEnumerator PlayerDeath(){
		camScript.Transition("Teleport",30);
		yield return null;
	}

	void TakeDamage(){
		if(!isDead){
			fireBullets = false;
			GameControl.isPlaying = false;
			StartCoroutine("PlayerDeath");
			AudioInfo.stopAudio = true;
			audio.PlayOneShot(deathClip);
			isDead = true;
		}
	}

	void Victory(){
		camScript.Transition("Teleport",30);
	}
		
	int currentCamSetting = 0;
	void Update(){

		if(!GameControl.isPlaying){
			if(Input.GetKeyDown(KeyCode.Space)){
				GameControl.victory = false;
				GameControl.NewGame();
				AudioInfo.stopAudio = false;
				Application.LoadLevel(Application.loadedLevel);
			}
			return;
		}

		if(GameControl.victory){
			Victory();
			GameControl.isPlaying = false;
		}

		if(Input.GetKeyDown(KeyCode.Space)){
			currentCamSetting = (currentCamSetting+1)%cameraArray.Length;
			if(cameraArray[currentCamSetting] == "Far"){
				camScript.Transition(cameraArray[currentCamSetting],100);
				canFire = false;
			}else{
				camScript.Transition(cameraArray[currentCamSetting],40);
				canFire = true;
			}
		}

		if(Input.GetKey(KeyCode.LeftShift)||Input.GetKey(KeyCode.RightShift)){
			boosting = true;
		}else{
			boosting = false;
		}

		float horizontal = Input.GetAxisRaw("Horizontal");
		float vertical = Input.GetAxisRaw("Vertical");

		rigidbody2D.AddForce(new Vector2(horizontal * Speed, vertical * Speed));
		
		vel = Mathf.Abs(rigidbody2D.velocity.magnitude) * 0.25f;
		
		if(prevVel > vel){
			float lerpVel = Mathf.Lerp(vel, 0, 0.25f);
			vel = lerpVel < vel ? lerpVel : vel;
		}
		
		//collider.radius = 0.5f+vel;
		
		prevVel = vel;
		
		Vector3 pos = Camera.main.WorldToScreenPoint (transform.position);
		Vector3 dir = Input.mousePosition - pos;
		float angle = Mathf.Atan2(dir.y,dir.x)*Mathf.Rad2Deg;
		targeter.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(pos.x,pos.y,9));
		targeter.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		
		if(Input.GetMouseButtonDown(0)){
			Debug.Log("Mouse Down");
			fireBullets = true;
			StartCoroutine("Fire");

		}
		if(Input.GetMouseButtonUp(0)){
			fireBullets = false;
		}
		
	}
}
