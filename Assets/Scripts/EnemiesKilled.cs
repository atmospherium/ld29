﻿using UnityEngine;
using System.Collections;

public class EnemiesKilled : MonoBehaviour {
	
	TextMesh textMesh;
	void Start(){
		textMesh = GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		textMesh.text = "Enemies Killed: "+GameControl.enemiesKilled+" / Previous Best: "+GameControl.enemiesPrevBest;
	}
}