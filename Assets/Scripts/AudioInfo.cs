﻿using UnityEngine;
using System.Collections;

public class AudioInfo : MonoBehaviour {
	int qSamples = 4096;  // array size (corresponds to about 85mS)
	
	private float[] samples; // audio samples

	public static float vol = 0;
	
	void Start () {
		samples = new float[qSamples];
	}

	public static bool stopAudio = false;
	
	float GetRMS(int channel) {
		audio.GetOutputData(samples, channel); // fill array with samples
		float sum = 0;
		for (var i=0; i < qSamples; i++){
			sum += samples[i]*samples[i]; // sum squared samples
		}
		return Mathf.Sqrt(sum/qSamples); // rms = square root of average
	}
	
	// when you want to measure the volume, call GetRMS for channels 0 and 1, and sum them:
	void Update(){
		if(stopAudio){
			audio.Stop();
		}
		vol = GetRMS(0) + GetRMS(1); // get left channel...
	}
}
