﻿using UnityEngine;
using System.Collections;

public class PressSpace : MonoBehaviour {

	TextMesh textMesh;
	void Awake(){
		textMesh = GetComponent<TextMesh>();
	}
	
	void Update(){
		if(GameControl.isPlaying){
			textMesh.text = "";
		}else if(GameControl.victory){
			textMesh.text = "Congratulations! You won!\nPress Space to Play Again";
		}else{
			textMesh.text = "Press Space to Try Again";
		}
	}
}
