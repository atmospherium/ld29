﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameControl : MonoBehaviour {

	public static bool isPlaying = true;
	public static bool victory = false;
	static bool callibrate = false;

	public static int enemiesKilled = 0;
	public static int enemiesPrevBest = 0;

	public static int enemiesRemaining = 1;

	void Awake(){
		victory = false;
		callibrate = true;
	}

	public static void RegisterKill(){
		enemiesKilled++;
	}

	public static void NewGame(){
		callibrate = true;
		enemiesPrevBest = enemiesKilled > enemiesPrevBest ? enemiesKilled : enemiesPrevBest;
		enemiesKilled = 0;
		enemiesRemaining = 1;
		isPlaying = true;

	}

	public static List<EnemySpawner> enemySpawners = null;
	void Callibrate(){
		enemySpawners = new List<EnemySpawner>();
		foreach(GameObject enemyController in GameObject.FindGameObjectsWithTag("EnemyControllers").ToList()){
			enemySpawners.Add(enemyController.GetComponent<EnemySpawner>());
		}
		if(enemySpawners == null || enemySpawners.Count==0){
			enemiesRemaining = 1;
			callibrate = true;
		}
	}

	// Update is called once per frame
	void Update () {
		if(victory == true){
			return;
		}
		if(callibrate){
			callibrate = false;
			Callibrate();
			return;
		}
		int enemyCount = 0;
		foreach(EnemySpawner enemySpawner in enemySpawners.ToList()){
			if(enemySpawner == null){
				enemySpawners.Remove(enemySpawner);
			}else{
				enemyCount += enemySpawner.enemiesRemaining;
			}
		}
		enemiesRemaining = enemyCount;
		if(enemiesRemaining == 0){
			victory = true;
		}
	}
}
